package com.example.mini_jeu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class apres_inscription extends AppCompatActivity {


    private Button btn_regles;
    private Button btn_jouer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_apres_inscription);

        btn_regles = findViewById(R.id.btn_regles);

        btn_regles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Regles = new Intent(apres_inscription.this, Regles.class);
                startActivity(Regles);
            }
        });

        btn_jouer = findViewById(R.id.btn_connexion);

        btn_jouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent connexion = new Intent(apres_inscription.this, Connexion.class);
                startActivity(connexion);
            }
        });
    }
}
