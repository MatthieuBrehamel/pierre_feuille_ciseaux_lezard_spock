package com.example.mini_jeu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;


public class Connexion extends AppCompatActivity {

    private Button btn_connexion;
    private EditText login;
    private EditText MDP;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connexion);

        login = findViewById(R.id.Login);
        MDP = findViewById(R.id.MDP);

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();

        btn_connexion = findViewById(R.id.btn_connexion);



            btn_connexion.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if(TextUtils.isEmpty(login.getText().toString()) && TextUtils.isEmpty(MDP.getText().toString())){
                        login.setError("Le login est requis.");
                        MDP.setError("Le mot de passe est requis");
                        return;
                    }
                    else if(TextUtils.isEmpty(login.getText().toString())) {
                        login.setError("Le login est requis.");
                        return;
                    }
                    else if(TextUtils.isEmpty(MDP.getText().toString())){
                        MDP.setError("Le mot de passe est requis");
                        return;
                    }
                    else {
                        signIn();
                    }
                }
            });


    }

    private void signIn() {

        mAuth.signInWithEmailAndPassword(login.getText().toString(), MDP.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            FirebaseUser user = mAuth.getCurrentUser();
                            String email = user.getEmail();
                            String name = user.getDisplayName();
                            Toast.makeText(Connexion.this, "Name : "+name+"\n"+"Email : "+email+"\n",
                                    Toast.LENGTH_SHORT).show();
                            Intent difficulte = new Intent(Connexion.this, Difficulte.class);
                            startActivity(difficulte);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(Connexion.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            // ...
                        }
                    }
                });
    }
}
