package com.example.mini_jeu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class Difficulte extends AppCompatActivity {

    private Button btn_regles;
    private Button btn_confirmer;
    private Button btn_classement;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_difficulte);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        final Spinner spinner = findViewById(R.id.spinner_difficulte);
        ArrayList<String> arrayList = new ArrayList<>();
        arrayList.add("Facile");
        arrayList.add("Normal");
        arrayList.add("Difficile");
        arrayList.add("INSANE");

        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this,android.R.layout.simple_spinner_item, arrayList);
        arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(arrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String tutorialsName = parent.getItemAtPosition(position).toString();
                mDatabase.child("Donnees").child("Difficulte").setValue(tutorialsName);
                Toast.makeText(parent.getContext(), "Selected: " + tutorialsName, Toast.LENGTH_LONG).show();
            }
            @Override
            public void onNothingSelected(AdapterView <?> parent) {
            }
        });

        btn_regles = findViewById(R.id.btn_regles);

        btn_regles.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent regles = new Intent(Difficulte.this, Regles.class);
                startActivity(regles);
            }
        });



        if(spinner == null)
        {
            Toast.makeText(getApplicationContext(),"Veuillez choisir une difficulté !", Toast.LENGTH_LONG).show();
        }
        else{
        btn_confirmer = findViewById(R.id.btn_confirmer);

        btn_confirmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Jeu = new Intent(Difficulte.this, Jeu.class);
                String valeur_difficulte = spinner.getSelectedItem().toString();
                Jeu.putExtra("String",valeur_difficulte );
                startActivity(Jeu);
            }
        });}

        btn_classement = findViewById(R.id.btn_classement);

        btn_classement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Classement = new Intent(Difficulte.this, Classement.class);
                startActivity(Classement);
            }
        });

        



    }
}
