package com.example.mini_jeu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class Gagne extends AppCompatActivity {

    private Button btn_rejouer;
    private Button btn_quitter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gagne);
        btn_rejouer = findViewById(R.id.btn_rejouer);

        btn_rejouer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent rejouer = new Intent(Gagne.this, Difficulte.class);
                startActivity(rejouer);
            }
        });

        btn_quitter = findViewById(R.id.btn_quitter);

        btn_quitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent quitter = new Intent(Gagne.this, MainActivity.class);
                startActivity(quitter);
            }
        });
    }
}
