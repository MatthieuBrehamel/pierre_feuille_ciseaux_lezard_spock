package com.example.mini_jeu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class Classement extends AppCompatActivity {

    private Button btn_menu;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_classement);

        mFirebaseDatabase = FirebaseDatabase.getInstance();
        DatabaseReference userDBRef = FirebaseDatabase.getInstance().getReference();
        userDBRef.child("USERS");

        btn_menu = findViewById(R.id.btn_menu);



        btn_menu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Menu = new Intent(Classement.this, MainActivity.class);
                    startActivity(Menu);
            }
        });
    }
}
