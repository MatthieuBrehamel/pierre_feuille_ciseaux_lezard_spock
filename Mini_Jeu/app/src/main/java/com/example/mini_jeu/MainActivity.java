package com.example.mini_jeu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private Button btn_inscription;
    private Button btn_connexion;
    private Button btn_classement;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        btn_connexion = findViewById(R.id.btn_connexion);

        btn_connexion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view2) {
                Intent Connexion = new Intent(MainActivity.this, Connexion.class);
                startActivity(Connexion);
                Toast.makeText(getApplicationContext(),"Ca Marche", Toast.LENGTH_LONG).show();
            }
        });

        btn_inscription = findViewById(R.id.btn_inscription);
        btn_inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Inscription = new Intent(MainActivity.this, Inscription.class);
                startActivity(Inscription);
            }
        });

        btn_classement = findViewById(R.id.btn_classement);
        btn_classement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Inscription = new Intent(MainActivity.this, Classement.class);
                startActivity(Inscription);
            }
        });

    }
}