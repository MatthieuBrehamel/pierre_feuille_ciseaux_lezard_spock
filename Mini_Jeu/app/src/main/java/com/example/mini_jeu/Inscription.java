package com.example.mini_jeu;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.ArrayList;
import java.util.List;

public class Inscription extends AppCompatActivity {

    private Button btn_conf_inscription;
    private EditText Nom;
    private EditText Prenom;
    private EditText Mail;
    private EditText Login;
    private EditText MotDePasse;
    private EditText ConfMDP;
    private Spinner Age;
    private RadioGroup rgSexe;
    private RadioButton Femme,Homme;
    private FirebaseDatabase mFirebaseDatabase;
    private FirebaseAuth mAuth;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mAuth = FirebaseAuth.getInstance();
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        setContentView(R.layout.activity_inscription);
        Nom = findViewById(R.id.Nom);
        Prenom = findViewById(R.id.Prenom);
        Mail = findViewById(R.id.Mail);
        Login = findViewById(R.id.Login);
        MotDePasse = findViewById(R.id.MotDePasse);
        ConfMDP = findViewById(R.id.ConfMDP);
        Age = (Spinner)findViewById(R.id.Age);
        rgSexe = findViewById(R.id.rb_Group);
        Femme = findViewById(R.id.Femme);
        Homme = findViewById(R.id.Homme);

        List age = new ArrayList<Integer>();
        for (int i = 8; i <= 100; i++) {
            age.add(Integer.toString(i));
        }
        ArrayAdapter<Integer> spinnerArrayAdapter = new ArrayAdapter<Integer>(
                this, android.R.layout.simple_spinner_item, age);
        spinnerArrayAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );

        Age.setAdapter(spinnerArrayAdapter);

        btn_conf_inscription = findViewById(R.id.btn_conf_inscription);

        btn_conf_inscription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(TextUtils.isEmpty(Nom.getText().toString()) && TextUtils.isEmpty(Prenom.getText().toString()) && TextUtils.isEmpty(Mail.getText().toString()) && TextUtils.isEmpty(Login.getText().toString()) && TextUtils.isEmpty(MotDePasse.getText().toString()) && TextUtils.isEmpty(ConfMDP.getText().toString())){
                    Nom.setError("Le Nom est requis.");
                    Prenom.setError("Le Prenom est requis");
                    Mail.setError("Le Mail est requis");
                    Login.setError("Le Login est requis");
                    MotDePasse.setError("Le Mot de Passe est requis");
                    ConfMDP.setError("Veuillez confirmer votre mot de passe");
                    return;
                }
                else if(TextUtils.isEmpty(Nom.getText().toString())) {
                    Nom.setError("Le Nom est requis.");
                    return;
                }
                else if(TextUtils.isEmpty(Prenom.getText().toString())){
                    Prenom.setError("Le Prenom est requis");
                    return;
                }
                else if(TextUtils.isEmpty(Mail.getText().toString())){
                    Mail.setError("Le Mail est requis");
                    return;
                }
                else if(TextUtils.isEmpty(Login.getText().toString())){
                    Login.setError("Le Login est requis");
                    return;
                }
                else if(TextUtils.isEmpty(MotDePasse.getText().toString())){
                    MotDePasse.setError("Le mot de passe est requis");
                    return;
                }
                else if(TextUtils.isEmpty(ConfMDP.getText().toString())){
                    ConfMDP.setError("Veuillez confirmer votre mot de passe");
                    return;
                }
                else if(!ConfMDP.getText().toString().equals(MotDePasse.getText().toString())){
                    MotDePasse.setError("Les mots de passe doivent-être identiques");
                    ConfMDP.setError("Les mots de passe doivent-être identiques");
                    return;
                }
                else {
                    signUp();
                }
            }
        });
    }

    private void signUp() {

        mAuth.createUserWithEmailAndPassword(Mail.getText().toString(), MotDePasse.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            signIn();
                            Intent Inscription = new Intent(Inscription.this, apres_inscription.class);
                            startActivity(Inscription);
                        } else {
                            // If sign in fails, display a message to the user.
                            Toast.makeText(Inscription.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    private void signIn() {

        mAuth.signInWithEmailAndPassword(Mail.getText().toString(), MotDePasse.getText().toString())
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            FirebaseUser user = mAuth.getCurrentUser();
                            String uid = user.getUid();

                            String nom = Nom.getText().toString();
                            String prenom = Prenom.getText().toString();
                            String email = Mail.getText().toString();
                            String login = Login.getText().toString();
                            String age = Age.getSelectedItem().toString();

                            int selectedId = rgSexe.getCheckedRadioButtonId();
                            RadioButton selectedRadioButton = (RadioButton) findViewById(selectedId);
                            String sexe = selectedRadioButton.getText().toString();

                            mDatabase.child("Donnees").child("USERS").child(uid).child("Prenom").setValue(prenom);
                            mDatabase.child("Donnees").child("USERS").child(uid).child("Nom").setValue(nom);
                            mDatabase.child("Donnees").child("USERS").child(uid).child("Age").setValue(age);
                            mDatabase.child("Donnees").child("USERS").child(uid).child("Login").setValue(login);
                            mDatabase.child("Donnees").child("USERS").child(uid).child("Email").setValue(email);
                            mDatabase.child("Donnees").child("USERS").child(uid).child("Sexe").setValue(sexe);
                            mDatabase.child("Donnees").child("USERS").child(uid).child("Points").setValue("0");
                            mDatabase.child("Donnees").child("USERS").child(uid).child("Classement").setValue("0");
                            Toast.makeText(Inscription.this, "Authentication success.",
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(Inscription.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }
}