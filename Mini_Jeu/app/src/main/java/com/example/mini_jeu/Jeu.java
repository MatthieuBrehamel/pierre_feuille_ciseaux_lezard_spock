package com.example.mini_jeu;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Random;

import static java.lang.Thread.sleep;

public class Jeu extends AppCompatActivity {

    private Button btn_regles;
    private Button btn_quitter;
    private TextView TW_difficulte;
    private TextView TW_score;
    private TextView TW_score2;
    private ImageButton btn_ciseaux;
    private ImageButton btn_papier;
    private ImageButton btn_pierre;
    private ImageButton btn_lezard;
    private ImageButton btn_spock;
    private ImageView Image_ordi;
    private ImageView Image_ordi2;
    private ImageView Image_ordi3;
    private FirebaseDatabase mFirebaseDatabase;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jeu);
        TW_difficulte = findViewById(R.id.TW_difficulte);
        TW_score = findViewById(R.id.TW_score);
        TW_score2 = findViewById(R.id.TW_score2);
        btn_ciseaux = findViewById(R.id.btn_ciseaux);
        btn_lezard = findViewById(R.id.btn_lezard);
        btn_papier = findViewById(R.id.btn_papier);
        btn_pierre = findViewById(R.id.btn_pierre);
        btn_spock = findViewById(R.id.btn_spock);
        Image_ordi = findViewById(R.id.Image_ordi);
        Image_ordi2 = findViewById(R.id.Image_ordi2);
        Image_ordi3 = findViewById(R.id.Image_ordi3);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        mFirebaseDatabase = FirebaseDatabase.getInstance();
        mDatabase = FirebaseDatabase.getInstance().getReference();
        Intent intent = getIntent();
        if (intent != null) {
            String donnees = "";
            if (intent.hasExtra("String")) { // vérifie qu'une valeur est associée à la clé “edittext”
                donnees = intent.getStringExtra("String"); // on récupère la valeur associée à la clé
                TW_difficulte.setText(donnees);
            }
            final int[] score_joueur = {0};
            final int[] score_ordi = {0};
            final int[] Ordi = {0};
            score_joueur[0] = 0;
            score_ordi[0] = 0;
            final String finalDonnees = donnees;
            btn_ciseaux.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view3) {
                    if (finalDonnees.equals("Facile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(5);

                     }
                    if (finalDonnees.equals("Normal")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 74)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 1;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 0;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                               Ordi[0] = 4;
                            }
                            else
                            {
                                Ordi[0] = 3;
                            }
                        }
                    }
                    if (finalDonnees.equals("Difficile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 49)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 1;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 0;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 4;
                            }
                            else
                            {
                                Ordi[0] = 3;
                            }
                        }
                    }
                    if (finalDonnees.equals("INSANE")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 24)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 1;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 0;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 4;
                            }
                            else
                            {
                                Ordi[0] = 3;
                            }
                        }
                    }
                    if ( Ordi[0] == 0)
                    {
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.shears2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if (Ordi[0] == 1 ) {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(R.drawable.lezard21);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 2)
                    {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.paper2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 3)
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.stone2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 4)
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(R.drawable.spock2);
                    }

                    if (score_joueur[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent gagne = new Intent(Jeu.this, Gagne.class);
                        startActivity(gagne);
                    }

                    if (score_ordi[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent perdu = new Intent(Jeu.this, Perdu.class);
                        startActivity(perdu);
                    }
                    mDatabase.child("Donnees").child("Points_Joueur").setValue(score_joueur[0]);
                    mDatabase.child("Donnees").child("Points_Ordi").setValue(score_ordi[0]);
                }
            });
            btn_lezard.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view3) {
                    if (finalDonnees.equals("Facile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(5);
                    }
                    if (finalDonnees.equals("Normal")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 74)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 1;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 3;
                            }
                        }
                    }
                    if (finalDonnees.equals("Difficile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 49)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 1;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 3;
                            }
                        }
                    }
                    if (finalDonnees.equals("INSANE")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 24)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 1;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 3;
                            }
                        }
                    }
                    if ( Ordi[0] == 0)
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.shears2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if (Ordi[0] == 1 )
                    {
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(R.drawable.lezard21);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 2)
                    {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.paper2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 3)
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.stone2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 4)
                    {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(R.drawable.spock2);
                    }
                    if (score_joueur[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent gagne = new Intent(Jeu.this, Gagne.class);
                        startActivity(gagne);
                    }
                    if (score_ordi[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent perdu = new Intent(Jeu.this, Perdu.class);
                        startActivity(perdu);
                    }
                    mDatabase.child("Donnees").child("Points_Joueur").setValue(score_joueur[0]);
                    mDatabase.child("Donnees").child("Points_Ordi").setValue(score_ordi[0]);
                }
            });
            btn_papier.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view3) {
                    if (finalDonnees.equals("Facile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(5);
                    }
                    if (finalDonnees.equals("Normal")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 74)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 3;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 1;
                            }
                        }
                    }
                    if (finalDonnees.equals("Difficile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 49)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 3;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 1;
                            }
                        }
                    }
                    if (finalDonnees.equals("INSANE")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 24)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 3;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 1;
                            }
                        }
                    }
                    if ( Ordi[0] == 0)
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.shears2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if (Ordi[0] == 1 )
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(R.drawable.lezard21);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 2)
                    {
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.paper2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 3)
                    {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.stone2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 4)
                    {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(R.drawable.spock2);
                    }
                    if (score_joueur[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent gagne = new Intent(Jeu.this, Gagne.class);
                        startActivity(gagne);
                    }
                    if (score_ordi[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent perdu = new Intent(Jeu.this, Perdu.class);
                        startActivity(perdu);
                    }
                    mDatabase.child("Donnees").child("Points_Joueur").setValue(score_joueur[0]);
                    mDatabase.child("Donnees").child("Points_Ordi").setValue(score_ordi[0]);
                }
            });
            btn_pierre.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view3) {
                    if (finalDonnees.equals("Facile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(5);
                    }
                    if (finalDonnees.equals("Normal")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 74)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 3;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 1;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                    }
                    if (finalDonnees.equals("Difficile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 49)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 3;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 1;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                    }
                    if (finalDonnees.equals("INSANE")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 24)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 3;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 1;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 2;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                    }

                    if ( Ordi[0] == 0)
                    {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.shears2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if (Ordi[0] == 1 )
                    {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(R.drawable.lezard21);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 2)
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.paper2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 3)
                    {
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.stone2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 4)
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(R.drawable.spock2);
                    }
                    if (score_joueur[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent gagne = new Intent(Jeu.this, Gagne.class);
                        startActivity(gagne);
                    }

                    if (score_ordi[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent perdu = new Intent(Jeu.this, Perdu.class);
                        startActivity(perdu);
                    }
                    mDatabase.child("Donnees").child("Points_Joueur").setValue(score_joueur[0]);
                    mDatabase.child("Donnees").child("Points_Ordi").setValue(score_ordi[0]);
                }
            });
            btn_spock.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view3) {
                    if (finalDonnees.equals("Facile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(5);
                    }
                    if (finalDonnees.equals("Normal")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 74)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 3;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 1;
                            }
                            else
                            {
                                Ordi[0] = 2;
                            }
                        }
                    }
                    if (finalDonnees.equals("Difficile")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 49)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 3;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 1;
                            }
                            else
                            {
                                Ordi[0] = 2;
                            }
                        }
                    }
                    if (finalDonnees.equals("INSANE")) {
                        Random rand = new Random();
                        Ordi[0] = rand.nextInt(99);
                        if(Ordi[0] <= 24)
                        {
                            Ordi[0] = rand.nextInt(2);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 3;
                            }
                            else if (Ordi[0] == 1)
                            {
                                Ordi[0] = 0;
                            }
                            else
                            {
                                Ordi[0] = 4;
                            }
                        }
                        else
                        {
                            Ordi[0] = rand.nextInt(1);
                            if(Ordi[0] == 0)
                            {
                                Ordi[0] = 1;
                            }
                            else
                            {
                                Ordi[0] = 2;
                            }
                        }
                    }
                    if ( Ordi[0] == 0)
                    {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.shears2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if (Ordi[0] == 1 )
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(R.drawable.lezard21);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 2)
                    {
                        score_ordi[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.paper2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 3)
                    {
                        score_joueur[0] += 1;
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(R.drawable.stone2);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(0);
                    }
                    else if ( Ordi[0] == 4)
                    {
                        TW_score.setText(String.valueOf(score_joueur[0]));
                        TW_score2.setText(String.valueOf(score_ordi[0]));
                        Image_ordi.setBackgroundResource(0);
                        Image_ordi2.setBackgroundResource(0);
                        Image_ordi3.setBackgroundResource(R.drawable.spock2);
                    }
                    if (score_joueur[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent gagne = new Intent(Jeu.this, Gagne.class);
                        startActivity(gagne);
                    }

                    if (score_ordi[0] == 3)
                    {
                        score_joueur[0] = 0;
                        score_ordi[0] = 0;
                        try {
                            sleep(500);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                        Intent perdu = new Intent(Jeu.this, Perdu.class);
                        startActivity(perdu);
                    }
                    mDatabase.child("Donnees").child("Points_Joueur").setValue(score_joueur[0]);
                    mDatabase.child("Donnees").child("Points_Ordi").setValue(score_ordi[0]);
                }
            });
        }
    btn_regles = findViewById(R.id.btn_regles);

        btn_regles.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent regles = new Intent(Jeu.this, Regles.class);
            startActivity(regles);
        }
    });

        btn_quitter = findViewById(R.id.btn_quitter);

        btn_quitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view2) {
                Intent Menu = new Intent(Jeu.this, MainActivity.class);
                startActivity(Menu);
            }
        });
    }
}
